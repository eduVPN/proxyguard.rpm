%dnl %global commit 46f0640c2f115ac4694cabf8bee63621a82b5723

Name:           proxyguard
Version:        2.0.0
Summary:        Proxy UDP traffic over TCP and HTTP(S)
Release:        1%{?dist}

License:        MIT
URL:            https://codeberg.org/eduVPN/proxyguard

%if %{defined commit}
Source0:        https://codeberg.org/eduVPN/proxyguard/archive/%{commit}.tar.gz
%else
Source0:        https://codeberg.org/eduVPN/proxyguard/releases/download/%{version}/proxyguard-%{version}.tar.xz
Source1:        https://codeberg.org/eduVPN/proxyguard/releases/download/%{version}/proxyguard-%{version}.tar.xz.minisig
Source2:        minisign-CA9409316AC93C07.pub
%endif 

Source3:        %{name}-client.service
Source4:        %{name}-client.sysconfig
Source5:        %{name}-server.service
Source6:        %{name}-server.sysconfig

# we have no desire to deal with "GOPATH", so we patch the whole thing out, 
# best idea ever!
Patch0:         %{name}-fix-imports.patch

BuildRequires:  minisign
BuildRequires:  systemd-rpm-macros
BuildRequires:  go-rpm-macros
%if 0%{?el9}
BuildRequires:  go-rpm-macros-epel
%endif

%description
Proxy UDP traffic over TCP and HTTP(S).

%package server
Summary: Proxy Server

%description server
Proxy UDP traffic over TCP and HTTP(S). It does this by doing an HTTP upgrade 
request similar to how websockets work. This means we can tunnel the protocol 
behind a reverse proxy.
 
%package client
Summary: Proxy Client

%description client
Proxy UDP traffic over TCP and HTTP(S). It does this by doing an HTTP upgrade 
request similar to how websockets work. This means we can tunnel the protocol 
behind a reverse proxy.

%prep
%if %{defined commit}
%setup -qn %{name}
%else
/usr/bin/minisign -V -m %{SOURCE0} -x %{SOURCE1} -p %{SOURCE2}
%setup -qn %{name}-%{version}
%endif
%patch -P0 -p1

%build
for CMD in cmd/*; do
  %gobuild -o _build/$(basename $CMD) $CMD/*.go
done

%install
install -m 0755 -vd            %{buildroot}%{_sbindir}
install -m 0755 -vp _build/*   %{buildroot}%{_sbindir}/
install -m 0755 -vd            %{buildroot}%{_unitdir}
install -m 0644 -vp %{SOURCE3} %{buildroot}%{_unitdir}/
install -m 0644 -vp %{SOURCE5} %{buildroot}%{_unitdir}/
install -m 0755 -vd            %{buildroot}%{_sysconfdir}/sysconfig
install -m 0644 -vp %{SOURCE6} %{buildroot}%{_sysconfdir}/sysconfig/%{name}-server
install -m 0644 -vp %{SOURCE4} %{buildroot}%{_sysconfdir}/sysconfig/%{name}-client

%check
%gotest

%post server
%systemd_post %{name}-server.service

%post client
%systemd_post %{name}-client.service

%preun server
%systemd_preun %{name}-server.service

%preun client
%systemd_preun %{name}-client.service

%postun server
%systemd_postun_with_restart %{name}-server.service

%postun client
%systemd_postun_with_restart %{name}-client.service

%files server
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}-server
%license LICENSE
%doc README.md
%{_sbindir}/%{name}-server
%{_unitdir}/%{name}-server.service

%files client
%config(noreplace) %{_sysconfdir}/sysconfig/%{name}-client
%license LICENSE
%doc README.md CHANGES.md
%{_sbindir}/%{name}-client
%{_unitdir}/%{name}-client.service

%changelog
* Fri Jan 31 2025 François Kooman <fkooman@tuxed.net> - 2.0.0-1
- update to 2.0.0
- remove upstreamed proxyguard-cleanup-wgconn.patch

* Mon Nov 25 2024 François Kooman <fkooman@tuxed.net> - 1.0.1-10
- Rocky Linux Go update

* Mon Nov 18 2024 François Kooman <fkooman@tuxed.net> - 1.0.1-9
- AlmaLinux Go update

* Wed Nov 06 2024 François Kooman <fkooman@tuxed.net> - 1.0.1-8
- apply PR#48

* Wed Sep 25 2024 François Kooman <fkooman@tuxed.net> - 1.0.1-7
- rebuild with new Go release

* Mon Jul 08 2024 François Kooman <fkooman@tuxed.net> - 1.0.1-6
- rebuild with new Go release

* Fri May 10 2024 François Kooman <fkooman@tuxed.net> - 1.0.1-5
- rebuild with new Go release

* Mon May 06 2024 François Kooman <fkooman@tuxed.net> - 1.0.1-4
- rebuild with new Go release

* Fri Apr 05 2024 François Kooman <fkooman@tuxed.net> - 1.0.1-3
- additional systemd proxyguard-server hardening

* Fri Apr 05 2024 François Kooman <fkooman@tuxed.net> - 1.0.1-2
- enable some systemd proxyguard-server hardening
- update systemd service descriptions

* Fri Apr 05 2024 François Kooman <fkooman@tuxed.net> - 1.0.1-1
- update to 1.0.1
- add CHANGES.md as documentation

* Fri Apr 05 2024 François Kooman <fkooman@tuxed.net> - 1.0.0-4
- run included unit tests

* Thu Apr 04 2024 François Kooman <fkooman@tuxed.net> - 1.0.0-3
- add go-rpm-macros as a BR

* Thu Apr 04 2024 François Kooman <fkooman@tuxed.net> - 1.0.0-2
- add minisign as a BR

* Thu Apr 04 2024 François Kooman <fkooman@tuxed.net> - 1.0.0-1
- initial package
